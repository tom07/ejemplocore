import { Component, OnInit, Input, Output, EventEmitter, ViewChild, ElementRef } from '@angular/core';
import { PedidoDetalle } from '../models/pedido-detalle';


@Component({
  selector: 'app-pedido-detalle',
  templateUrl: './pedido-detalle.component.html',
  styleUrls: ['./pedido-detalle.component.sass']
})
export class PedidoDetalleComponent implements OnInit {
  @Input() pedidoDetalle: PedidoDetalle[] = new Array<PedidoDetalle>();
  @Output() seEliminoUnProducto = new EventEmitter();
 
  constructor() { 
    
  }

  ngOnInit() {
   
  }

  eliminar(posicion: number)
  {

   this.seEliminoUnProducto.emit({ index: posicion })
  }



}
