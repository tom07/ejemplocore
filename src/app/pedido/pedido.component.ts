import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Pedido } from '../models/pedido';
import Swal from 'sweetalert2'
@Component({
  selector: 'app-pedido',
  templateUrl: './pedido.component.html',
  styleUrls: ['./pedido.component.sass']
})
export class PedidoComponent implements OnInit {
  pedido: Pedido = new Pedido();
  @ViewChild('buscador', {static: true}) buscador: ElementRef
  constructor() { 
   

  }

  ngOnInit() {
    this.pedido.cliente = 'Juan'
    this.pedido.direccion = 'SPS'
    
  }


  agregarProducto()
  {
    this.pedido.pedidoDetalle.push({
      cantidad: 20,
      precio: 15,
      producto: 'Agua',
      total: 300
    })
    Swal.fire(
      {
        title: 'Producto agregado',
        text: 'Se agrego correctamente',
        type: 'success'
      }
    )
  }


  elHijoEliminoAlgo(evento)
  {
    this.pedido.pedidoDetalle.splice(evento.index, 1);
    Swal.fire({
      title: 'Producto Eliminado',
      text:'Se elimino correctamente',
      type: 'warning'
    })
  }

  ngAfterViewInit()
  {

    this.buscador.nativeElement.focus()
  }
  // ngDoCheck()
  // {
  //   console.log('Ejecutaste algo')
  // }


  ngOnDestroy()
  {
    this.pedido.pedidoDetalle.length = 0;
    Swal.fire({
      title: 'Estas abandondo los pedidos'
    })
  }
}
