import { Component, ViewChild, ElementRef } from '@angular/core';
import { HijoComponent } from './hijo/hijo.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass']
})
export class AppComponent {
  title = 'ejemplosCore';
  @ViewChild('inputNombre', { static: true}) inputNombre: ElementRef;
  @ViewChild(HijoComponent, { static: true }) hijo: HijoComponent;

  mostrar()
  {
    // console.log(this.inputNombre)
    // // this.inputNombre.nativeElement.style.background = "red"
    // this.inputNombre.nativeElement.select();
    this.hijo.titulo = "Lo modifique desde el padre"
  }

}
